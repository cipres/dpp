dpp
===

Deploy python packages to IPFS.

GitLab CI
---------

You will need to declare the following CI variables:

- *IPFS_RPS_ENDPOINT*: URL of the IPFS remote pinning service
- *IPFS_RPS_KEY*: Token for the remote pinning service

Your *.gitlab-ci.yml* should include the *dwheel.yml* template:

```yaml
variables:
  WHEEL_NAME: wheelname
  VERSION: 0.0.1

include:
  remote: 'https://gitlab.com/cipres/dpp/-/raw/master/.gitlab/templates/dwheel.yml'
```
